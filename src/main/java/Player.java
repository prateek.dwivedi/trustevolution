public class Player{

    private MoveBehaviour moveBehaviour;
    private int finalScore;

    public Player(MoveBehaviour type) {
        moveBehaviour = type;
    }

    public int getFinalScore() {
        return finalScore;
    }

    public MoveType move() {
        return moveBehaviour.move();
    }

    void incrementScore(int score) {
        finalScore += score;
    }
}
