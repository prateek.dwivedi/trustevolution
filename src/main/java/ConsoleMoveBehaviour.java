import java.util.Scanner;

public class ConsoleMoveBehaviour  implements MoveBehaviour {

    private Scanner moveInput;

    public MoveType move() {
        String move = moveInput.next();
        if (move.equals("1"))
            return MoveType.COOPERATE;
        else if (move.equals("2"))
            return MoveType.CHEAT;
        return null;
    }

    public ConsoleMoveBehaviour(Scanner input) {
        this.moveInput = input;
    }
}
