import java.io.PrintStream;

public class Game {
    private Player player1, player2;
    private Machine machine;
    private PrintStream print;


    public Game(Player player1, Player player2, Machine machine, PrintStream print) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.print = print;
    }

    public void play(int numberOfRounds) {
        for (int i = 0; i < numberOfRounds; i++) {
            Score score = machine.score(player1.move(), player2.move());
            player1.incrementScore(score.getPlayer1Score());
            player2.incrementScore(score.getPlayer2Score());
        }
        print.println(player1.getFinalScore() + " | " + player2.getFinalScore());
    }

}

