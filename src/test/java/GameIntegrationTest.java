import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Scanner;

import static org.mockito.Mockito.mock;

public class GameIntegrationTest {
    @Test
    public void shouldReturnCumulativeScore()
    {
        Player player1 = new Player(new ConsoleMoveBehaviour(new Scanner("1\n1\n1")));
        Player player2 = new Player(new ConsoleMoveBehaviour(new Scanner("1\n1\n1")));

        Machine machine = new Machine();
        PrintStream mockPrintStream = mock(PrintStream.class);

        Game game = new Game(player1,player2,machine,mockPrintStream);
        game.play(3);

        Assert.assertEquals(6 , player1.getFinalScore());

        Assert.assertEquals(6 , player2.getFinalScore());

    }
}
