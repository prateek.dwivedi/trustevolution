import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GameTest {
    @Test
    public void shouldCheckForFirstRoundWhenBothCooperate() {

        Player mockPlayer1 = mock(Player.class);
        when(mockPlayer1.move()).thenReturn(MoveType.COOPERATE);

        Player mockPlayer2 = mock(Player.class);
        when(mockPlayer2.move()).thenReturn(MoveType.COOPERATE);

        PrintStream mockPrintStream = mock(PrintStream.class);

        Score score = new Score(2, 2);

        Machine mockMachine = mock(Machine.class);
        when(mockMachine.score(MoveType.COOPERATE, MoveType.COOPERATE)).thenReturn(score);


        Game game = new Game(mockPlayer1, mockPlayer2, mockMachine, mockPrintStream);
        game.play(1);

        Mockito.verify(mockPlayer1, Mockito.times(1)).move();
        Mockito.verify(mockPlayer2, Mockito.times(1)).move();
        Mockito.verify(mockMachine, Mockito.times(1)).score(MoveType.COOPERATE, MoveType.COOPERATE);
        Mockito.verify(mockPrintStream, Mockito.times(1)).println(score);

    }

    @Test
    public void  shouldCheckForManyRound(){

        Player mockPlayer1 = mock(Player.class);
        when(mockPlayer1.move()).thenReturn(MoveType.COOPERATE);

        Player mockPlayer2 = mock(Player.class);
        when(mockPlayer2.move()).thenReturn(MoveType.COOPERATE);

        PrintStream printStream = mock(PrintStream.class);

        Score score = new Score(2, 2);

        Machine mockMachine = mock(Machine.class);
        when(mockMachine.score(MoveType.COOPERATE, MoveType.COOPERATE)).thenReturn(score);


        Game game = new Game(mockPlayer1, mockPlayer2, mockMachine, printStream);
        game.play(5);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(mockPlayer1, Mockito.times(5)).move();
        Mockito.verify(mockPlayer2, Mockito.times(5)).move();
        Mockito.verify(mockMachine, Mockito.times(5)).score(MoveType.COOPERATE, MoveType.COOPERATE);
        Mockito.verify(printStream).println(captor.capture());


        String actualValue = (String) captor.getValue();
        Assert.assertEquals("10 | 10", actualValue);

    }
}
